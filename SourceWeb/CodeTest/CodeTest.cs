﻿using NUnit.Framework;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CodeTest
{
    public class CodeTest
    {
        [Test]
        public static void Main(string[] args)
        {
            var foo = new Foo();
            Task.Factory.StartNew(() => foo.Second(CodeTest.PrintSecond));
            Task.Factory.StartNew(() => foo.First(CodeTest.PrintFirst));
            Task.Factory.StartNew(() => foo.Third(CodeTest.PrintThird));

            var test = new ZeroEvenOdd(200);
            Task.Factory.StartNew(() => test.Zero(CodeTest.PrintNumber));
            Task.Factory.StartNew(() => test.Even(CodeTest.PrintNumber));
            Task.Factory.StartNew(() => test.Odd(CodeTest.PrintNumber));

            Console.Read();
        }
        public static void PrintFirst()
        {
            Console.WriteLine("输出printFirst");
        }
        public static void PrintSecond()
        {
            Console.WriteLine("输出printSecond");
        }
        public static void PrintThird()
        {
            Console.WriteLine("输出Third");
        }

        public static void PrintNumber(int x)
        {
            Console.WriteLine($"输出{x}");
        }
    }
    public class Foo
    {
        private AutoResetEvent event2 = new AutoResetEvent(false);
        private AutoResetEvent event3 = new AutoResetEvent(false);
        public Foo()
        {

        }

        public void First(Action printFirst)
        {

            // printFirst() outputs "first". Do not change or remove this line.
            printFirst();
            event2.Set();
        }

        public void Second(Action printSecond)
        {
            event2.WaitOne();
            // printSecond() outputs "second". Do not change or remove this line.
            printSecond();
            event3.Set();
        }

        public void Third(Action printThird)
        {
            event3.WaitOne();
            // printThird() outputs "third". Do not change or remove this line.
            printThird();
        }
    }

    public class ZeroEvenOdd
    {
        private int n;

        private AutoResetEvent event_zero = new AutoResetEvent(true);
        private AutoResetEvent oddEvent = new AutoResetEvent(false);
        private AutoResetEvent evenEvent = new AutoResetEvent(false);

        public ZeroEvenOdd(int n)
        {
            this.n = n;
        }

        // printNumber(x) outputs "x", where x is an integer.
        public void Zero(Action<int> printNumber)
        {
            for (int i = 1; i <= n; i++)
            {
                event_zero.WaitOne();
                printNumber(0);
                if (i == 1)
                {
                    oddEvent.Set();
                }
                else if ((i % 2) > 0)
                {
                    oddEvent.Set();
                }
                else
                {
                    evenEvent.Set();
                }
            }
        }

        public void Even(Action<int> printNumber)
        {
            for (int i = 1; i <= n; i++)
            {
                if (i % 2 == 0)
                {
                    evenEvent.WaitOne();
                    printNumber(i);
                    event_zero.Set();
                }
            }
        }

        public void Odd(Action<int> printNumber)
        {
            for (int i = 1; i <= n; i++)
            {
                if (i % 2 > 0)
                {
                    oddEvent.WaitOne();
                    printNumber(i);
                    event_zero.Set();
                }
            }
        }
    }

    public class H2O
    {
        private SemaphoreSlim h = new SemaphoreSlim(2, 2);
        private SemaphoreSlim o = new SemaphoreSlim(0, 1);
        public H2O()
        {

        }

        public void Hydrogen(Action releaseHydrogen)
        {
            h.Wait();
            // releaseHydrogen() outputs "H". Do not change or remove this line.
            releaseHydrogen();
            if (h.CurrentCount == 0)
            {
                h.Release();
            }
        }

        public void Oxygen(Action releaseOxygen)
        {
            o.Wait();
            // releaseOxygen() outputs "O". Do not change or remove this line.
            releaseOxygen();
            h.Release(2);
        }
    }
}
